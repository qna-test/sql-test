## QUESTION
---
Berikut adalah tabel mahasiswa, tabel mata kuliah, dan tabel kuliah dalam format Markdown:

**Tabel Mahasiswa:**

| id      | name           |
| ------- | -------------- |
| 2022001 | Lionel Messi   |
| 2022002 | Cristiano Ronaldo |
| 2022003 | Erling Haaland |
| 2022004 | Kylian Mbappe  |

**Tabel Mata Kuliah:**

| semester | id_mhsw | code_matkul |
| -------- | ------- | ----------- |
| 1        | 2022002 | MKU003      |
| 1        | 2022004 | MKU004      |
| 1        | 2022004 | MKU001      |
| 1        | 2022002 | MKU002      |
| 1        | 2022001 | MKU005      |
| 1        | 2022003 | MKU002      |
| 1        | 2022003 | MKU001      |
| 1        | 2022001 | MKU003      |
| 2        | 2022002 | MKU003      |
| 2        | 2022001 | MKU004      |
| 2        | 2022001 | MKU005      |
| 2        | 2022002 | MKU005      |

## Dari tabel-tabel diatas buatlah query dengan SQL :
- Menampilkan mahasiswa dengan semua mata kuliah yang diambil
- Menampilkan mahasiswa yang tidak mengambil mata kuliah di semester 2
- Menampilkan mata kuliah yang tidak ada yang mengambil di semester 2
- Menampilkan semua mata kuliah dan jumlah mahasiswa yang mengambil di semester 1

---
## ANSWER
---
![sql1.png](quest%2Fsql1.png)
![sql2.png](quest%2Fsql2.png)

1. **Menampilkan mahasiswa dengan semua mata kuliah yang diambil**:
```sql
SELECT m.id, m.name, k.code_matkul
FROM Mahasiswa m
         LEFT JOIN MataKuliah k ON m.id = k.id_mhsw
ORDER BY m.id, k.code_matkul;
```
![result-1.png](result%2Fresult-1.png)


2. **Menampilkan mahasiswa yang tidak mengambil mata kuliah di semester 2**:
```sql
SELECT m.id, m.name
FROM Mahasiswa m
WHERE m.id NOT IN (
    SELECT DISTINCT id_mhsw
    FROM MataKuliah
    WHERE semester = 2
)
ORDER BY m.id;
```
![result-2.png](result%2Fresult-2.png)

3. **Menampilkan mata kuliah yang tidak ada yang mengambil di semester 2**:
```sql
SELECT mt.code_matkul
FROM MataKuliah mt
WHERE mt.code_matkul NOT IN (
    SELECT DISTINCT code_matkul
    FROM MataKuliah
    WHERE semester = 2
);
```
![result-3.png](result%2Fresult-3.png)

4. **Menampilkan semua mata kuliah dan jumlah mahasiswa yang mengambil di semester 1**:
```sql
SELECT k.code_matkul, COUNT(DISTINCT k.id_mhsw) as jumlah_mahasiswa
FROM MataKuliah k
WHERE k.semester = 1
GROUP BY k.code_matkul
ORDER BY k.code_matkul;
```
![result-4.png](result%2Fresult-4.png)