CREATE TABLE Mahasiswa (
                           id VARCHAR(10) PRIMARY KEY,
                           name VARCHAR(255)
);


INSERT INTO Mahasiswa (id, name) VALUES
                                     ('2022001', 'Lionel Messi'),
                                     ('2022002', 'Cristiano Ronaldo'),
                                     ('2022003', 'Erling Haaland'),
                                     ('2022004', 'Kylian Mbappe');


CREATE TABLE MataKuliah (
                            semester INT,
                            id_mhsw VARCHAR(10),
                            code_matkul VARCHAR(10)
);


INSERT INTO MataKuliah (semester, id_mhsw, code_matkul) VALUES
                                                            (1, '2022002', 'MKU003'),
                                                            (1, '2022004', 'MKU004'),
                                                            (1, '2022004', 'MKU001'),
                                                            (1, '2022002', 'MKU002'),
                                                            (1, '2022001', 'MKU005'),
                                                            (1, '2022003', 'MKU002'),
                                                            (1, '2022003', 'MKU001'),
                                                            (1, '2022001', 'MKU003'),
                                                            (2, '2022002', 'MKU003'),
                                                            (2, '2022001', 'MKU004'),
                                                            (2, '2022001', 'MKU005'),
                                                            (2, '2022002', 'MKU005');
